/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_r_list = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  renderNode: function(node_data) {
    return $('<li><span class="node--label">' + node_data.name + '</span></li>')
      .data('node_id', node_data.id);
  },

  toggleNode: function($graph_area, node_id) {
    var $node_area = Drupal.behaviors.graphs_r_list.findNodeArea($graph_area, node_id);
    var $subtree_area = Drupal.behaviors.graphs_r_list.getSubtreeArea($node_area);
    $subtree_area.toggle();
  },

  findNodeArea: function($graph_area, node_id, children_only = false) {
    if (children_only) {
      return $graph_area.children('li').filter( 
        function() {
          return ($(this).data("node_id") == node_id);
        }
      );
    }
    else {
      return $graph_area.find('li').filter( 
        function() {
          return ($(this).data("node_id") == node_id);
        }
      );
    }
  },
  
  /**
   * Returns the first UL element (jQuery object) of a node.
   *
   * It returns the UL element that contains the node subtree and creates one
   * if no UL element were found.
   */
  getSubtreeArea: function($node_area) {
    var $subtree_area = $node_area.children('ul:first');
    if (!$subtree_area.length) {
      // Not found, add one.
      $subtree_area = $('<ul></ul>').appendTo($node_area);
    }
    return $subtree_area;
  },

  /**
   * Adds or complete a subtree for the given graph at the given node.
   *
   */
  addSubtreeAt: function(graph_index, node_id, graph_data) {
    var graph = Drupal.settings.graphs.graphs[graph_index];
    var sourcer = Drupal.settings.graphs.sourcers[graph.sourcer];
    var renderer = Drupal.settings.graphs.renderers[graph.renderer];
    var $graph_area = $('#graphs_' + graph_index);
    var $node_area = Drupal.behaviors.graphs_r_list.findNodeArea(
      $graph_area,
      node_id
    );
    var $subtree_area = Drupal.behaviors.graphs_r_list.getSubtreeArea($node_area);

    var parent_of_regex = new RegExp(
      (renderer.parent_of_relationships.length ?
        '^(?:' + renderer.parent_of_relationships.join('|') + ')$'
        : ''),
      'i'
    );
    var child_of_regex = new RegExp(
      (renderer.child_of_relationships.length ?
        '^(?:' + renderer.child_of_relationships.join('|') + ')$'
        : ''),
      'i'
    );

    // Gather relationships.
    var links = [];

    // Loop on all links from the given node to others.
    for (var target_id in sourcer.link_index[node_id]) {
      // Skip loop if the property is from prototype.
      if (!sourcer.link_index[node_id].hasOwnProperty(target_id)) {
        continue;
      }
      for (var relationship in sourcer.link_index[node_id][target_id]) {
        // Skip loop if the property is from prototype.
        if (!sourcer.link_index[node_id][target_id].hasOwnProperty(relationship)) {
          continue;
        }

        // @todo: Prevent infinite loop for cyclic data.

        // Skip non-matching relationships.
        if ((!relationship && !parent_of_regex)
            || relationship.match(parent_of_regex)) {
          links.push({'target_id': target_id, 'relationship': relationship});
        }
      }
    }

    // Loop on all links that connect to the given node.
    for (var target_id in sourcer.link_index) {
      // Skip loop if the property is from prototype or not related to our node.
      if (!sourcer.link_index.hasOwnProperty(target_id)
          || !sourcer.link_index[target_id][node_id]) {
        continue;
      }

      for (var relationship in sourcer.link_index[target_id][node_id]) {
        // Skip loop if the property is from prototype.
        if (!sourcer.link_index[target_id][node_id].hasOwnProperty(relationship)) {
          continue;
        }

        // @todo: Prevent infinite loop for cyclic data.

        // Skip non-matching relationships.
        if ((!relationship && !child_of_regex)
            || relationship.match(child_of_regex)) {
          links.push({'target_id': target_id, 'relationship': relationship});
        }
      }
    }

    // Process gathered relationships.
    links.forEach(function (link) {
      // Make sure the node is not already present.
      if (!Drupal.behaviors.graphs_r_list.findNodeArea($subtree_area, link.target_id, true).length) {
        Drupal.behaviors.graphs_r_list
          .renderNode(
            sourcer.node_index[link.target_id]
          )
          .appendTo($subtree_area)
          .children('span.node--label:first')
            .on('click', function () {
              var from_node_id = $(this).parent().data('node_id');
              if ($(this).not('.processed-node').length) {
                $(this).addClass('processed-node');
                Drupal.behaviors.graphs.getGraph(
                  graph_index,
                  graph.sourcer,
                  {
                    graph_id: graph.id,
                    node_id: from_node_id,
                    depth: (renderer.default_depth ? renderer.default_depth : 1),
                    rel_types: (renderer.parent_of_relationships ? renderer.parent_of_relationships : []),
                    back_rel_types: (renderer.child_of_relationships ? renderer.child_of_relationships : []),
                    callback: function (new_graph_data) {
                      Drupal.behaviors.graphs_r_list.addSubtreeAt(
                        graph_index,
                        from_node_id,
                        new_graph_data
                      );
                    }
                  }
                );
              }
              else {
                // Toggle children.
                Drupal.behaviors.graphs_r_list.toggleNode($graph_area, from_node_id);
              }
            })
        ;
      }
    });

  },
  
  /**
   * Renders initial list.
   */
  render: function (graph_index, rdr_index) {
    var graph = Drupal.settings.graphs.graphs[graph_index];
    var sourcer = Drupal.settings.graphs.sourcers[graph.sourcer];
    var renderer = Drupal.settings.graphs.renderers[graph.renderer];

    try {
      // Get initial graph.
      Drupal.behaviors.graphs.getGraph(
        graph_index,
        graph.sourcer,
        {
          graph_id: graph.id,
          depth: (renderer.default_depth ? renderer.default_depth : 1),
          rel_types: (renderer.parent_of_relationships ? renderer.parent_of_relationships : []),
          back_rel_types: (renderer.child_of_relationships ? renderer.child_of_relationships : []),
          callback: function (graph_data) {
            var $graph_area = $('#graphs_' + graph_index)
              .addClass('graphs_r_list');
            // Build and display the list.
            var $graph_node_area = $("<ul></ul>\n").appendTo($graph_area);
            var $root = Drupal.behaviors.graphs_r_list.renderNode(graph_data.nodes[0])
              .appendTo($graph_node_area);
            Drupal.behaviors.graphs_r_list.addSubtreeAt(
              graph_index,
              graph_data.nodes[0].id,
              graph_data
            );
            // Mark all node with children as already processed.
            $root.find('li:has(li)').addClass('processed-node');
          }
        }
      );
    }
    catch (error) {
      alert(error.message);
    }
  }
};

}(jQuery));
